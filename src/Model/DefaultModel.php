<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class DefaultModel extends AbstractModel{

    protected static $table = 'contact';

    protected $id;
    protected $sujet;
    protected $email;
    protected $message;

    public function getId(){
        return $this->id;
    }

    public function getSujet(){
        return $this->sujet;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getMessage(){
        return $this->message;
    }

    public static function getAllContactOrderBy($column = 'created_at',$order = 'DESC'){
        return App::getDatabase()->query("SELECT * FROM ".self::$table. " ORDER BY $column $order" ,get_called_class());
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (sujet,email,message,created_at) VALUES (?,?,?,NOW())",array($post['sujet'],$post['email'],$post['message']));
    }

}