<?php

namespace App\Controller;

use App\Model\DefaultModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class DefaultController extends AbstractController{

    private $v;

    public function __construct(){
        $this->v = new Validation();
    }

    public function index()
    {
        $message = 'Bienvenue sur le framework MVC';
        //$this->dump($message);
        $this->render('app.default.frontpage',array(
            'message' => $message,
        ));
    }

    public function contact(){
        $errors = [];

        if (!empty($_POST['submitted'])){
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                DefaultModel::insert($post);
                $this->addFlash('success', 'Votre demande a bien été envoyée.');
                $this->redirect('');
            }
        }
        $form = new Form($errors);

        $this->render('app.default.contact',array(
            'form' => $form,
        ));
    }

    public function listing(){

        $contacts = DefaultModel::getAllContactOrderBy();

        $this->render('app.default.listing',array(
            'contacts' => $contacts,
        ));

    }

    public function validate($v,$post){
        $errors = [];
        $errors['sujet'] = $v->textValid($post['sujet'], 'sujet',2, 150);
        $errors['email'] = $v->emailValid($post['email'],'email');
        $errors['message'] = $v->textValid($post['message'], 'message',2, 500);
        return $errors;
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
