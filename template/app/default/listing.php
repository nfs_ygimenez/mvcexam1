<section id="listing">
    <div class="wrap">
        <h1>Liste des contacts</h1>
        <table>
            <thead>
            <tr>
                <th>Envoyé le</th>
                <th>Email</th>
                <th>Sujet</th>
                <th>Message</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($contacts as $contact){ ?>
                <tr>
                    <td><?php echo date('d/M/Y à H:i:s',strtotime($contact->created_at)); ?></td>
                    <td><a href="mailto:<?php echo $contact->email; ?>"><?php echo $contact->email; ?></a></td>
                    <td><?php echo $contact->sujet; ?></td>
                    <td><?php echo $contact->message; ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</section>